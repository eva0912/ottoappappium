package com.example.test;

import com.example.page.AirPage;
import com.example.page.ConfirmationPage;
import com.example.page.HomePage;
import org.testng.annotations.Test;

public class AirTrxTest extends TestBase{
    String billerNumbervalid = "11118800990000";
    @Test
    public void testAirTrxSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        AirPage airPage = new AirPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());

        homePage.clickSeeAllPpob();
        Thread.sleep(3000);
        homePage.clickMenuAir();
        airPage.clickDenom("PAM JAYA");
        airPage.inputBillerNumber(billerNumbervalid);
        airPage.clickButtonnext();
        confirmationPage.clickButtonNext();

    }
}
