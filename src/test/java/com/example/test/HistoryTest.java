package com.example.test;

import com.example.page.*;
import org.testng.annotations.Test;

public class HistoryTest extends TestBase{
    @Test
    public void testHistoryListSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        HistoryPage historyPage = new HistoryPage(getDriver());

        homePage.clickMenuHistory();
        historyPage.isListLayoutVisible();
        Thread.sleep(5000);
    }

    @Test
    public void testLidtHistoryisEmpty() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        HistoryPage historyPage = new HistoryPage(getDriver());

        homePage.clickMenuHistory();
        historyPage.isEmptyLayoutVisible();
        Thread.sleep(5000);
    }
}
