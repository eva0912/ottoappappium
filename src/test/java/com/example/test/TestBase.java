package com.example.test;

import com.example.setup.AndroidSetup;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.AfterEach;
import org.testng.annotations.*;

public class TestBase {
    protected AndroidDriver<AndroidElement> driver;

    public AndroidDriver<AndroidElement> getDriver() {
        return driver;
    }
    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        driver = AndroidSetup.openApps();
    }

    @AfterMethod
    public void tearDown() {
        AndroidSetup.tearDown();
    }
}
