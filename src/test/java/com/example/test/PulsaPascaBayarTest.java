package com.example.test;

import com.example.page.ConfirmationPage;
import com.example.page.HomePage;
import com.example.page.PinTrxPage;
import com.example.page.PulsaPaketDataPage;
import org.testng.annotations.Test;

public class PulsaPascaBayarTest extends TestBase{
    String phoneNumberValid = "082291414781";
    String phoneNumberInvalid = "082291414781";

    @Test
    public void testPulsaPostPaidSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PulsaPaketDataPage pulsaPaketDataPage = new PulsaPaketDataPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        PinTrxPage pinTrxPage = new PinTrxPage(getDriver());
        Thread.sleep(7000);
        homePage.clickMenuPulsaPakeData();
//        Thread.sleep(3000);
        pulsaPaketDataPage.choosetabPascabayar();
        pulsaPaketDataPage.inputPhoneNumber(phoneNumberValid);
        pulsaPaketDataPage.clickButtonNext();
        confirmationPage.clickButtonNext();
        pinTrxPage.inputPin("111111");
        Thread.sleep(5000);
    }
    @Test
    public void testPulsaPostPaidWrongBilleNumber() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PulsaPaketDataPage pulsaPaketDataPage = new PulsaPaketDataPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        PinTrxPage pinTrxPage = new PinTrxPage(getDriver());
        Thread.sleep(7000);
        homePage.clickMenuPulsaPakeData();
//        Thread.sleep(3000);
        pulsaPaketDataPage.choosetabPascabayar();
        pulsaPaketDataPage.inputPhoneNumber(phoneNumberInvalid);
        pulsaPaketDataPage.clickButtonNext();
        confirmationPage.clickButtonNext();
        pinTrxPage.inputPin("111111");
        Thread.sleep(5000);

    }
}
