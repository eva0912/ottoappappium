package com.example.test;

import com.example.page.HomePage;
import com.example.page.LoginPage;
import com.example.page.OTPPage;
import com.example.page.RegisterPage;
import org.testng.annotations.Test;

public class LoginTest extends TestBase {
    public static final long WAIT = 10;
    String phoneNumberRegistered = "081300030333";
    String phoneNumberUnregistered = "081300030011";

    @Test
    public void loginSuccess() throws InterruptedException {
        Thread.sleep(7000);
        HomePage homePage = new HomePage(getDriver());
        LoginPage loginPage = new LoginPage(getDriver());
        OTPPage otpPage = new OTPPage(getDriver());
        homePage.clickButtonLogin();
        loginPage.inputPhoneNumber(phoneNumberRegistered);
        loginPage.clickButtonNext();
        Thread.sleep(3000);
        otpPage.inputOtp("555555");
        Thread.sleep(10000);
    }
    @Test
    public void loginWithPhoneNumberNotRegistered() throws InterruptedException {
        Thread.sleep(7000);
        HomePage homePage = new HomePage(getDriver());
        LoginPage loginPage = new LoginPage(getDriver());
        RegisterPage registerPage = new RegisterPage(getDriver());
        homePage.clickButtonLogin();
        loginPage.inputPhoneNumber(phoneNumberUnregistered);
        loginPage.clickButtonNext();
        registerPage.isEmailDisplayed();

        Thread.sleep(3000);
    }
}
