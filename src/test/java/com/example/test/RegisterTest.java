package com.example.test;

import com.example.page.HomePage;
import com.example.page.LoginPage;
import com.example.page.OTPPage;
import com.example.page.RegisterPage;
import org.testng.annotations.Test;

public class RegisterTest extends TestBase{
    String phoneNumber = "081294092000";
    String emailValid = "lia@gmail.com";
    String emailInvalid = "lia@gmail";
    String nameValid = "Liani";
    String nameInvalid = "Li";
    @Test
    public void testRegisterSuccess() throws InterruptedException {
        LoginPage loginPage = new LoginPage(getDriver());
        HomePage homePage = new HomePage(getDriver());
        RegisterPage registerPage = new RegisterPage(getDriver());
        OTPPage otpPage = new OTPPage(getDriver());

        homePage.clickButtonLogin();
        loginPage.inputPhoneNumber(phoneNumber);
        loginPage.clickButtonNext();
        registerPage.inputNama(nameValid);
        registerPage.inputEmail(emailValid);
        registerPage.pressBtnNext();
        registerPage.scrollingTncRegister();
        registerPage.pressBtnSignUp();
        String otpReg = registerPage.getOtp(phoneNumber);
        Thread.sleep(3000);
        otpPage.inputOtp(otpReg);
        Thread.sleep(10000);

    }
    @Test
    public void testNameLessthan4() {
        LoginPage loginPage = new LoginPage(getDriver());
        HomePage homePage = new HomePage(getDriver());
        RegisterPage registerPage = new RegisterPage(getDriver());

        homePage.clickButtonLogin();
        loginPage.inputPhoneNumber(phoneNumber);
        loginPage.clickButtonNext();
        registerPage.inputNama(nameInvalid);
        registerPage.inputEmail(nameValid);
        registerPage.pressBtnNext();
        registerPage.isEmailDisplayed();

    }

}
