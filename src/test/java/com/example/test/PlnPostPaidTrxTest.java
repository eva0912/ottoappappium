package com.example.test;

import com.example.page.HomePage;
import com.example.page.PlnPage;
import com.example.page.PlnPostPaidPage;
import org.testng.annotations.Test;

public class PlnPostPaidTrxTest extends TestBase{
    String billerNumberValid = "12340000990000";
    String billerNumberInvalid = "12340000990001";
    @Test
    public void testPlnPostPaidSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PlnPage plnPage = new PlnPage(getDriver());
        PlnPostPaidPage plnPostPaidPage = new PlnPostPaidPage(getDriver());

        homePage.clickMenuListrik();
        plnPage.clickPlnPostpaid();
        plnPostPaidPage.inputBillerNumber(billerNumberValid);
        plnPostPaidPage.clickButtonnext();

    }@Test
    public void testPlnPostPaid_invalidNumber() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PlnPage plnPage = new PlnPage(getDriver());
        PlnPostPaidPage plnPostPaidPage = new PlnPostPaidPage(getDriver());

        homePage.clickMenuListrik();
        plnPage.clickPlnPostpaid();
        plnPostPaidPage.inputBillerNumber(billerNumberInvalid);
        plnPostPaidPage.clickButtonnext();

    }
}
