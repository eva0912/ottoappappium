package com.example.test;

import com.example.page.*;
import org.testng.annotations.Test;

public class WithDrawOttoGrowTest extends TestBase{
    String nominalValid = "10000";
    String nominalLessThan10000 = "5000";
    @Test
    public void tesWithDrawOttoGrowSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        FinancialPage financialPage = new FinancialPage(getDriver());
        InvestmentPage investmentPage = new InvestmentPage(getDriver());
        OttoGrowPage ottoGrowPage = new OttoGrowPage(getDriver());
        WithDrawOttoGrowPage withDrawOttoGrowPage = new WithDrawOttoGrowPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());

        homePage.clickSeeAllFinancial();
        financialPage.clickInvestment();
        investmentPage.clickOttoGrow();
        ottoGrowPage.clickMenuTopUp();
        withDrawOttoGrowPage.inputNominalIur(nominalValid);
        withDrawOttoGrowPage.clickBtnNext();
        confirmationPage.clickButtonNext();

    }
    @Test
    public void tesWithDrawOttoGrowlessThan10000() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        FinancialPage financialPage = new FinancialPage(getDriver());
        InvestmentPage investmentPage = new InvestmentPage(getDriver());
        OttoGrowPage ottoGrowPage = new OttoGrowPage(getDriver());
        WithDrawOttoGrowPage withDrawOttoGrowPage = new WithDrawOttoGrowPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());

        homePage.clickSeeAllFinancial();
        financialPage.clickInvestment();
        investmentPage.clickOttoGrow();
        ottoGrowPage.clickMenuTopUp();
        withDrawOttoGrowPage.inputNominalIur(nominalLessThan10000);
        withDrawOttoGrowPage.isMessageErrorDisplayed();

    }

    @Test
    public void tesWithDrawOttoGrowMoreThanSaldo() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        FinancialPage financialPage = new FinancialPage(getDriver());
        InvestmentPage investmentPage = new InvestmentPage(getDriver());
        OttoGrowPage ottoGrowPage = new OttoGrowPage(getDriver());
        WithDrawOttoGrowPage withDrawOttoGrowPage = new WithDrawOttoGrowPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());

        homePage.clickSeeAllFinancial();
        financialPage.clickInvestment();
        investmentPage.clickOttoGrow();
        ottoGrowPage.clickMenuTopUp();
        withDrawOttoGrowPage.inputNominalIur(nominalLessThan10000);
        withDrawOttoGrowPage.isMessageErrorDisplayed();

    }
}
