package com.example.test;

import com.example.page.*;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;

public class TransferTemanTest extends TestBase{
    String phoneNumberValid = "081300031112";
    String amount = "10000";
    @Description("Testing the login functionality")
    @Feature("Login")
    @Story("Valid Login")
    @Test
    public void testTransferTemanSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        OttoCashPage ottoCashPage = new OttoCashPage(getDriver());
        TranferPage tranferPage = new TranferPage(getDriver());
        TransferTemanPage transferTemanPage = new TransferTemanPage(getDriver());
        TransferTemanNominalPage transferTemanNominalPage = new TransferTemanNominalPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        PinTrxPage pinTrxPage = new PinTrxPage(getDriver());
        Thread.sleep(7000);

        homePage.clickMenuOttoCash();
        ottoCashPage.clickMenuTransfer();
        tranferPage.clickTabTemanOtto();
        transferTemanPage.inputBillerNumber(phoneNumberValid);
        transferTemanPage.clickButtonSubmit();
        transferTemanNominalPage.inputAmountTransfer(amount);
        transferTemanNominalPage.clickButtonSubmit();
        confirmationPage.clickButtonNext();
        pinTrxPage.inputPin("111111");
        Thread.sleep(5000);
    }
}
