package com.example.test;

import com.example.page.*;
import org.testng.annotations.Test;

public class TokenTest extends TestBase{

    @Test
    public void testGenerateTokenSucces(){
        HomePage homePage = new HomePage(getDriver());
        QrisPage qrisPage = new QrisPage(getDriver());
        OTPPage otpPage = new OTPPage(getDriver());

        homePage.clickTabQris();
        qrisPage.clickTabToken();
        otpPage.inputOtp("111111");

    }
}
