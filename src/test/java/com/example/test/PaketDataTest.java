package com.example.test;

import com.example.page.ConfirmationPage;
import com.example.page.HomePage;
import com.example.page.PinTrxPage;
import com.example.page.PulsaPaketDataPage;
import org.testng.annotations.Test;

public class PaketDataTest extends TestBase{

    @Test(groups = "sanity")
    public void testDataSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PulsaPaketDataPage pulsaPaketDataPage = new PulsaPaketDataPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        PinTrxPage pinTrxPage = new PinTrxPage(getDriver());
        Thread.sleep(10000);
        homePage.clickMenuPulsaPakeData();
//        Thread.sleep(3000);
        pulsaPaketDataPage.choosetabData();
        pulsaPaketDataPage.inputPhoneNumber("085230112643");
//        pulsaPaketDataPage.tabDenomPulsa("TELKOMSEL 20000");
//        pulsaPaketDataPage.clickButtonNext();
//        confirmationPage.clickButtonNext();
//        pinTrxPage.inputPin("111111");
        Thread.sleep(5000);
    }

    @Test(groups = "smoke")
    public void testDataWrongPin() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PulsaPaketDataPage pulsaPaketDataPage = new PulsaPaketDataPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        PinTrxPage pinTrxPage = new PinTrxPage(getDriver());
        Thread.sleep(7000);
        homePage.clickMenuPulsaPakeData();
//        Thread.sleep(3000);
        pulsaPaketDataPage.choosetabData();
        pulsaPaketDataPage.inputPhoneNumber("085230112644");
        pulsaPaketDataPage.tabDenomPulsa("TELKOMSEL 20000");
        pulsaPaketDataPage.clickButtonNext();
//        confirmationPage.clickButtonNext();
        pinTrxPage.inputPin("121212");
        Thread.sleep(5000);
    }
}
