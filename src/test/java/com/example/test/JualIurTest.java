package com.example.test;

import com.example.page.*;
import org.testng.annotations.Test;

public class JualIurTest extends TestBase{
    HomePage homePage = new HomePage(getDriver());
    FinancialPage financialPage = new FinancialPage(getDriver());
    InvestmentPage investmentPage = new InvestmentPage(getDriver());
    IurPage iurPage = new IurPage(getDriver());
    JualIurPage jualIurPage = new JualIurPage(getDriver());
    ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
    @Test
    public void testJualIurSebagianSuccess() throws InterruptedException {


        homePage.clickSeeAllFinancial();
        financialPage.clickInvestment();
        investmentPage.clickIur();
        iurPage.clickMoreIur();
        Thread.sleep(3000);
        iurPage.clickJualIur();
        jualIurPage.inputNominalIur("10000");
        jualIurPage.clickBtnNext();
        confirmationPage.clickButtonNext();
        Thread.sleep(5000);

    }

    @Test
    public void testJualIurSemuaSuccess() throws InterruptedException {
//        HomePage homePage = new HomePage(getDriver());
//        FinancialPage financialPage = new FinancialPage(getDriver());
//        InvestmentPage investmentPage = new InvestmentPage(getDriver());
//        IurPage iurPage = new IurPage(getDriver());
//        JualIurPage jualIurPage = new JualIurPage(getDriver());
//        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());

        homePage.clickSeeAllFinancial();
        financialPage.clickInvestment();
        investmentPage.clickIur();
        iurPage.clickMoreIur();
        Thread.sleep(3000);
        iurPage.clickJualIur();
        jualIurPage.clickBtnSeluruhnya();
        jualIurPage.clickBtnNext();
        confirmationPage.clickButtonNext();
        Thread.sleep(5000);

    }
}
