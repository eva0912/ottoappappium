package com.example.test;

import com.example.page.*;
import org.testng.annotations.Test;

public class PlnPrepaidTrxTest extends TestBase{
    String billerNumberValid = "12340000990000";
    String billerNumberInvalid = "12340000990001";
    String denom = "TOKEN 50000";
    @Test
    public void testPlnPrepaidSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PlnPage plnPage = new PlnPage(getDriver());
        PlnPrepaidPage plnPrepaidPage = new PlnPrepaidPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        ReceiptPage receiptPage = new ReceiptPage(getDriver());

        homePage.clickMenuListrik();
        plnPage.clickPlnPrepaid();
        plnPrepaidPage.inputBillerNumber(billerNumberValid);
        plnPrepaidPage.clickPanahGetDenom();
        plnPrepaidPage.chooseDenom(denom);
        plnPrepaidPage.clickbtnNext();
        confirmationPage.clickButtonNext();
        Thread.sleep(5000);

    }
    @Test
    public void testPlnPrepaidFailed_invalidNumber() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PlnPage plnPage = new PlnPage(getDriver());
        PlnPrepaidPage plnPrepaidPage = new PlnPrepaidPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        ReceiptPage receiptPage = new ReceiptPage(getDriver());

        homePage.clickMenuListrik();
        plnPage.clickPlnPrepaid();
        plnPrepaidPage.inputBillerNumber(billerNumberInvalid);
        plnPrepaidPage.clickPanahGetDenom();
        plnPrepaidPage.chooseDenom(denom);

    }
}
