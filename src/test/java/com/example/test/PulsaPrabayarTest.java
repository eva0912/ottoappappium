package com.example.test;

import com.example.page.*;
import org.testng.annotations.Test;

public class PulsaPrabayarTest extends TestBase{

    @Test
    public void testPulsaPrepaidSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        PulsaPaketDataPage pulsaPaketDataPage = new PulsaPaketDataPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        PinTrxPage pinTrxPage = new PinTrxPage(getDriver());
        Thread.sleep(10000);
        homePage.clickMenuPulsaPakeData();
//        Thread.sleep(3000);
        pulsaPaketDataPage.inputPhoneNumber("085530112643");
        pulsaPaketDataPage.tabDenomPulsa("20000");
        pulsaPaketDataPage.clickButtonNext();
        confirmationPage.clickButtonNext();
        pinTrxPage.inputPin("111111");
        Thread.sleep(5000);
    }

}
