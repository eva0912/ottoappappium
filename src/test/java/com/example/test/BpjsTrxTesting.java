package com.example.test;

import com.example.page.BpjsPage;
import com.example.page.ConfirmationPage;
import com.example.page.HomePage;
import com.example.page.PinTrxPage;
import org.testng.annotations.Test;

public class BpjsTrxTesting extends TestBase{
    String billerNumberValid = "8801000040408";
    String denom = "Maret 2024";
    @Test
    public void testBpjsTrxSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        BpjsPage bpjsPage = new BpjsPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        PinTrxPage pinTrxPage = new PinTrxPage(getDriver());

        homePage.clickMenuBpjs();
        bpjsPage.inputBillerNumber(billerNumberValid);
        bpjsPage.clickPanahGetDenom();
        bpjsPage.chooseDenom(denom);
        bpjsPage.clickButtonBuy();
        confirmationPage.clickButtonNext();
        pinTrxPage.inputPin("111111");
        Thread.sleep(5000);
    }
    @Test
    public void testBpjsTrxNotEnoughBalance() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        BpjsPage bpjsPage = new BpjsPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());
        PinTrxPage pinTrxPage = new PinTrxPage(getDriver());

        homePage.clickMenuBpjs();
        bpjsPage.inputBillerNumber(billerNumberValid);
        bpjsPage.clickPanahGetDenom();
        bpjsPage.chooseDenom(denom);
        bpjsPage.clickButtonBuy();
        confirmationPage.clickButtonNext();
        pinTrxPage.inputPin("111111");
        Thread.sleep(5000);
    }
}
