package com.example.test;

import com.example.page.*;
import org.testng.annotations.Test;

public class TopUpOttoGrowTest extends TestBase{
    String nominalValid = "10000";
    String nominalLessThan10000 = "5000";
    @Test
    public void testTopUpOttoGrowSuccess() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        FinancialPage financialPage = new FinancialPage(getDriver());
        InvestmentPage investmentPage = new InvestmentPage(getDriver());
        OttoGrowPage ottoGrowPage = new OttoGrowPage(getDriver());
        TopUpOttoGrowPage topUpOttoGrowPage = new TopUpOttoGrowPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());

        homePage.clickSeeAllFinancial();
        financialPage.clickInvestment();
        investmentPage.clickOttoGrow();
        ottoGrowPage.clickMenuTopUp();
        topUpOttoGrowPage.inputNominalIur(nominalValid);
        topUpOttoGrowPage.clickBtnNext();
        confirmationPage.clickButtonNext();

    }
    @Test
    public void testTopUpOttoGrowLessThan10000() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        FinancialPage financialPage = new FinancialPage(getDriver());
        InvestmentPage investmentPage = new InvestmentPage(getDriver());
        OttoGrowPage ottoGrowPage = new OttoGrowPage(getDriver());
        TopUpOttoGrowPage topUpOttoGrowPage = new TopUpOttoGrowPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());

        homePage.clickSeeAllFinancial();
        financialPage.clickInvestment();
        investmentPage.clickOttoGrow();
        ottoGrowPage.clickMenuTopUp();
        topUpOttoGrowPage.inputNominalIur(nominalLessThan10000);

    }
    @Test
    public void testTopUpOttoGrowMoreThanSaldo() throws InterruptedException {
        HomePage homePage = new HomePage(getDriver());
        FinancialPage financialPage = new FinancialPage(getDriver());
        InvestmentPage investmentPage = new InvestmentPage(getDriver());
        OttoGrowPage ottoGrowPage = new OttoGrowPage(getDriver());
        TopUpOttoGrowPage topUpOttoGrowPage = new TopUpOttoGrowPage(getDriver());
        ConfirmationPage confirmationPage = new ConfirmationPage(getDriver());

        homePage.clickSeeAllFinancial();
        financialPage.clickInvestment();
        investmentPage.clickOttoGrow();
        ottoGrowPage.clickMenuTopUp();
        topUpOttoGrowPage.inputNominalIur("10000");

    }
}
