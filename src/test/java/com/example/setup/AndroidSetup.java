package com.example.setup;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.URL;

public class AndroidSetup {
    public static AndroidDriver<AndroidElement> driver;

    public static DesiredCapabilities initDevice(){
        File filePath = new File(System.getProperty("user.dir"));
        File app = new File(filePath, "/apk/ottoapp.apk");
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("automationName","UiAutomator2");
        cap.setCapability("platformName","Android");
        cap.setCapability("deviceName","RR8T3029ZVY");
        cap.setCapability("appPackage","com.pede.emoney");
        cap.setCapability("appActivity",".ui.activity.splash.SplashActivity");
        cap.setCapability("app",app.getAbsolutePath());
        cap.setCapability("autoGrantPermissions","true");
        cap.setCapability("noReset","true");
        return cap;
    }
    public static AndroidDriver<AndroidElement> openApps() throws Exception {
        String appiumServerURL = "http://0.0.0.0:4723/wd/hub";
        Thread.sleep(5000);
        driver = new AndroidDriver(new URL(appiumServerURL), initDevice());
        System.out.println("Apps started...");
        return driver;
    }

    public static void tearDown() {
        if (driver != null) {
            driver.closeApp();
            System.out.println("Apps closed...");
        }
    }

}
