package com.example.page;

import com.example.test.TestBase;
import com.example.utils.TestUtils;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    public static AndroidDriver<AndroidElement> driver;

    public BasePage(AndroidDriver<AndroidElement> driver_){
        driver = driver_;
    }
    public void click(MobileElement e) {
        waitForVisibility(e);
        e.click();
    }

    public void sendKeys(MobileElement e, String txt) {
        waitForVisibility(e);
        e.sendKeys(txt);
    }

    //scroll
    public MobileElement andScrollToElementUsingUiScrollable(String childLocAttr, String childLocValue) {
        return (MobileElement) ((FindsByAndroidUIAutomator) driver).findElementByAndroidUIAutomator(
                "new UiScrollable(new UiSelector()" + ".scrollable(true)).scrollIntoView("
                        + "new UiSelector()."+ childLocAttr +"(\"" + childLocValue + "\"));");
    }

    public void tapMenuByTextContains(String data) throws InterruptedException {
        WebElement webElement = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+data+"\"))");
        webElement.click();
        Thread.sleep(2000);
    }

    public void clickByUISelectorId(String id) throws InterruptedException {
        WebElement webElement = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().resourceId(\""+id+"\"))");
        webElement.click();
        Thread.sleep(3000);
    }


    public void inputTextById(MobileElement element, String input){
//        if(isElementPresentById(id, timeout)) {
//            findElementWithId(id).sendKeys(input);
//        }
        element.sendKeys(input);
    }
    private AndroidElement findElementWithId(String id) {
        return driver.findElementById(id);
    }

    public boolean isElementPresentById(String id, long timeOut) {
        By by = By.id(id);
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeOut);
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isElementPresentByMobileElement(MobileElement mobileElement) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(mobileElement));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public void waitForVisibility(MobileElement e) {
        WebDriverWait wait = new WebDriverWait(driver, TestUtils.WAIT);
        wait.until(ExpectedConditions.visibilityOf(e));
    }

    public String getOtp( String phoneNumber){
        Response response;
        RequestSpecification request = RestAssured.given();

        request.header("Content-Type", "application/json");
        response = request.when().get("https://gambit-dev.ottodigital.id/api/v1/otto-users/get-otp?mobilePhoneNumber="+phoneNumber);

        String jsonString = response.getBody().asString();
        System.out.println(jsonString);
        String status = JsonPath.from(jsonString).get("data.otp");
        System.out.println(status);
        return JsonPath.from(jsonString).get("data.otp");
    }
}
