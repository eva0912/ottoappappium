package com.example.page;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class InsurancePage extends BasePage{
    public InsurancePage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
}
