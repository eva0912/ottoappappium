package com.example.page;

import groovy.util.logging.Log;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class PlnPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/recycler_view")
    private MobileElement listView;
    public PlnPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void clickPlnPrepaid() throws InterruptedException {
        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/recycler_view"));

        // Find all child elements (items) within the RecyclerView
        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/tv_name"));
        if(!items.isEmpty()){
            items.get(0).click();
            System.out.println("Ada"+ items.size());
        }else{
            System.out.println("Tidak ada");
        }
        Thread.sleep(5000);
    }

    public void clickPlnPostpaid() throws InterruptedException {
        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/recycler_view"));

        // Find all child elements (items) within the RecyclerView
        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/tv_name"));
        if(!items.isEmpty()){
            items.get(1).click();
            System.out.println("Ada"+ items.size());
        }else{
            System.out.println("Tidak ada");
        }
        Thread.sleep(5000);
    }
}
