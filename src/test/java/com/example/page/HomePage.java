package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HomePage extends BasePage{

    @AndroidFindBy(id = "com.pede.emoney:id/ottoWalletRegBtn")
    private MobileElement buttonLogin;

    @AndroidFindBy(id = "com.pede.emoney:id/ottoFinancialAllBtn")
    private MobileElement ottoFinancialAllBtn;

    @AndroidFindBy(id = "com.pede.emoney:id/ottoPpobAllBtn")
    private MobileElement ottoPpobAllBtn;

    @AndroidFindBy(id = "com.pede.emoney:id/hitzone1")
    private MobileElement btnOttoCash;

    @AndroidFindBy(id = "com.pede.emoney:id/img_history")
    private MobileElement img_history;
    @AndroidFindBy(id = "com.pede.emoney:id/tab_home")
    private MobileElement tab_home;
    @AndroidFindBy(id = "com.pede.emoney:id/tab_qrcode")
    private MobileElement tab_qris;
    @AndroidFindBy(id = "com.pede.emoney:id/tab_profile")
    private MobileElement tab_profile;
    public HomePage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
    public void clickMenuPulsaPakeData() throws InterruptedException {
        clickItemListPpob(0);
    }
    public void clickMenuBpjs() throws InterruptedException {
        clickItemListPpob(2);
    }
    public void clickMenuListrik() throws InterruptedException {
        clickItemListPpob(1);
    }
    public void clickTabHome() {
        click(tab_home);
    }
    public void clickTabQris() {
        click(tab_qris);
    }
    public void clickTabProfile() {
        click(tab_profile);
    }
    public void clickButtonLogin(){
        click(buttonLogin);
    }

    public void clickSeeAllFinancial() throws InterruptedException {
        clickByUISelectorId("com.pede.emoney:id/ottoFinancialAllBtn");
    }
    public void clickSeeAllPpob(){
        click(ottoPpobAllBtn);
    }

    public void clickMenuAir() throws InterruptedException {
//        tapMenuByTextContains("Air");
        clickItemListMoreMenuPpob(2);
    }

    public void clickMenuOttoCash() throws InterruptedException {
        click(btnOttoCash);
        Thread.sleep(3000);
    }
    public void clickMenuHistory() throws InterruptedException {
        click(img_history);
        Thread.sleep(3000);
    }

    public void clickItemListPpob(int position) throws InterruptedException {
        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/ottoPpobRecycler"));

        // Find all child elements (items) within the RecyclerView
        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/item_layout"));
        if(!items.isEmpty()){
            items.get(position).click();
        }else{
            System.out.println("Tidak ada");
        }
        Thread.sleep(5000);
    }

    public void clickItemListMoreMenuPpob(int position) throws InterruptedException {
        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/recycler_view_main_menu"));

        // Find all child elements (items) within the RecyclerView
        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/item_layout"));
        if(!items.isEmpty()){
            items.get(position).click();
        }else{
            System.out.println("Tidak ada");
        }
        Thread.sleep(5000);
    }
}
