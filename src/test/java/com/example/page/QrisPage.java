package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class QrisPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/btn_tab_1")
    private MobileElement btn_tab_1;
    @AndroidFindBy(id = "com.pede.emoney:id/btn_tab_2")
    private MobileElement btn_tab_2;

    public QrisPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
    public void clickTabQris(){
        click(btn_tab_1);
    }
    public void clickTabToken(){
        click(btn_tab_2);
    }
}
