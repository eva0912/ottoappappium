package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ProfilePage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/cv_otto_cash")
    private MobileElement btnOttoCash;
    @AndroidFindBy(id = "com.pede.emoney:id/cv_otto_grow")
    private MobileElement btnOttoGrow;
    @AndroidFindBy(id = "com.pede.emoney:id/btn_logout")
    private MobileElement btnLogOut;

    public ProfilePage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void clickMenuOttoCash(){
        click(btnOttoCash);
    }
    public void clickMenuOttoGrow(){
        click(btnOttoGrow);
    }
    public void clickHubungiOtto() throws InterruptedException {
        clickItemListDocumentOtto(0);
    }
    public void clickSyaratKetentuan() throws InterruptedException {
        clickItemListDocumentOtto(1);
    }
    public void clickKebijkanPrivasi() throws InterruptedException {
        clickItemListDocumentOtto(2);
    }
    public void clickTentangOtto() throws InterruptedException {
        clickItemListDocumentOtto(3);
    }

    public void clickItemListDocumentOtto(int position) throws InterruptedException {
        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/recycler_view"));

        // Find all child elements (items) within the RecyclerView
        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/item_layout"));
        if(!items.isEmpty()){
            items.get(position).click();
        }else{
            System.out.println("Tidak ada");
        }
        Thread.sleep(5000);
    }


}
