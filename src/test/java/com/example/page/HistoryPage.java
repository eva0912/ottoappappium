package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class HistoryPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/content_layout")
    private MobileElement content_layout;

    @AndroidFindBy(id = "com.pede.emoney:id/empty_layout")
    private MobileElement empty_layout;
    public HistoryPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void isEmptyLayoutVisible(){
        isElementPresentByMobileElement(empty_layout);
    }

    public void isListLayoutVisible(){
        isElementPresentByMobileElement(content_layout);
    }
}
