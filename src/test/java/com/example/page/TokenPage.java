package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class TokenPage extends BasePage{

    @AndroidFindBy(id = "com.pede.emoney:id/btn_buy")
    private MobileElement buttonNext;
    @AndroidFindBy(id = "com.pede.emoney:id/et_input_biller")
    private MobileElement etPhoneNumber;

    public TokenPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
    public void clickSeeAllPpob(){
//        click(ottoPpobAllBtn);
    }
}
