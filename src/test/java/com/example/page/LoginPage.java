package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage{

    @AndroidFindBy(id = "com.pede.emoney:id/ottoEdtLogin")
    private MobileElement editTextLogin;
    @AndroidFindBy(id = "com.pede.emoney:id/btNext")
    private MobileElement btnNext;
    public LoginPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void inputPhoneNumber(String phoneNumber){
        inputTextById(editTextLogin, phoneNumber);
    }

    public void clickButtonNext(){
        click(btnNext);
    }

}
