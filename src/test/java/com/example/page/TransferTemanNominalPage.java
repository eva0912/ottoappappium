package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class TransferTemanNominalPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/et_amount")
    private MobileElement et_amount;
    @AndroidFindBy(id = "com.pede.emoney:id/btn_submit")
    private MobileElement btnSubmit;
    public TransferTemanNominalPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void inputAmountTransfer(String amount){
        inputTextById(et_amount, amount);
    }

    public void clickButtonSubmit() throws InterruptedException {
        click(btnSubmit);
        Thread.sleep(5000);
    }

}
