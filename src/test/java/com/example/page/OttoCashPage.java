package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class OttoCashPage extends BasePage{
    public OttoCashPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void clickMenuTransfer() throws InterruptedException {
        tapMenuByTextContains("Transfer");
//        clickItemList(0);
        Thread.sleep(5000);
    }
    public void clickMenuTarikDuit() throws InterruptedException {
        clickItemList(1);
    }
    public void clickMenuTopUp() throws InterruptedException {
        clickItemList(2);
        Thread.sleep(5000);
    }
    public void clickMenuUbanPin() throws InterruptedException {
        clickItemList(3);
        Thread.sleep(5000);
    }

    public void clickItemList(int item) throws InterruptedException {
        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/recycler_view"));

        // Find all child elements (items) within the RecyclerView
        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/item_layout"));
        if(!items.isEmpty()){
            items.get(item).click();
        }
        Thread.sleep(5000);
    }
}
