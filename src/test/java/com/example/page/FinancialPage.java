package com.example.page;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class FinancialPage extends BasePage{

    public FinancialPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void clickPinjaman() throws InterruptedException {
        tapMenuByTextContains("Pinjaman");
    }
    public void clickInvestment() throws InterruptedException {
        tapMenuByTextContains("Investasi");
    }
    public void clickAsuransi() throws InterruptedException {
        tapMenuByTextContains("Asuransi");
    }

}
