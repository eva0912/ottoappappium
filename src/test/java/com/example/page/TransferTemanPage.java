package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class TransferTemanPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/et_input_biller")
    private MobileElement et_inputBiller;
    @AndroidFindBy(id = "com.pede.emoney:id/btn_submit")
    private MobileElement btnSubmit;

    public TransferTemanPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void inputBillerNumber(String phoneNumber_valid){
        inputTextById(et_inputBiller, phoneNumber_valid);
    }

    public void clickButtonSubmit() throws InterruptedException {
        click(btnSubmit);
        Thread.sleep(5000);
    }

}
