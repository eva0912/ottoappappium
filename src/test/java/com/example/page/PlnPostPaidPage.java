package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class PlnPostPaidPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/et_payment_code")
    private MobileElement et_billerNumber;

    @AndroidFindBy(id = "com.pede.emoney:id/btn_submit")
    private MobileElement btnNext;
    public PlnPostPaidPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
    public void inputBillerNumber(String billNumber){
        inputTextById(et_billerNumber, billNumber);
    }
    public void clickButtonnext(){
        click(btnNext);
    }
}
