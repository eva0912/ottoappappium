package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage extends BasePage{

    @AndroidFindBy(id = "com.pede.emoney:id/et_name")
    private MobileElement etName;
    @AndroidFindBy(id = "com.pede.emoney:id/et_email")
    private MobileElement etEmail;
    @AndroidFindBy(id = "com.pede.emoney:id/btn_next")
    private MobileElement btnNext;
    @AndroidFindBy(id = "com.pede.emoney:id/btn_signup")
    private MobileElement btnSignUp;

    public RegisterPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void inputNama(String name){
        inputTextById(etName, name);
    }
    public void inputEmail(String email){
        inputTextById(etEmail, email);
    }
    public void isEmailDisplayed(){
        isElementPresentById("com.pede.emoney:id/et_email", 5000);
    }
    public void pressBtnNext(){
        click(btnNext);
    }
    public void pressBtnSignUp(){
        click(btnSignUp);
    }
    public void scrollingTncRegister() throws InterruptedException {
        andScrollToElementUsingUiScrollable("textContains", "sepihak jika terindikasi dan");
        Thread.sleep(2000);
    }

}
