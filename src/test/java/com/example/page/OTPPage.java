package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class OTPPage extends BasePage{

    @AndroidFindBy(id = "com.pede.emoney:id/et_otp_1")
    private MobileElement etOtp1;
    @AndroidFindBy(id = "com.pede.emoney:id/et_otp2")
    private MobileElement etOtp2;
    @AndroidFindBy(id = "com.pede.emoney:id/et_otp3")
    private MobileElement etOtp3;
    @AndroidFindBy(id = "com.pede.emoney:id/et_otp4")
    private MobileElement etOtp4;
    @AndroidFindBy(id = "com.pede.emoney:id/et_otp5")
    private MobileElement etOtp5;
    @AndroidFindBy(id = "com.pede.emoney:id/et_otp")
    private MobileElement etOtp;
    public OTPPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void inputOtp(String otp){
        sendKeys(etOtp, String.valueOf(otp.charAt(0)));
        sendKeys(etOtp, String.valueOf(otp.charAt(1)));
        sendKeys(etOtp, String.valueOf(otp.charAt(2)));
        sendKeys(etOtp, String.valueOf(otp.charAt(3)));
        sendKeys(etOtp, String.valueOf(otp.charAt(4)));
        sendKeys(etOtp, String.valueOf(otp.charAt(5)));
    }
}
