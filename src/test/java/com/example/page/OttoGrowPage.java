package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class OttoGrowPage extends BasePage{
    public OttoGrowPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
    public void clickMenuTopUp() throws InterruptedException {
        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/recycler_view_main_menu"));

        // Find all child elements (items) within the RecyclerView
        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/item_layout"));
        if(!items.isEmpty()){
            items.get(0).click();
        }else{
            System.out.println("Tidak ada");
        }
        Thread.sleep(5000);
    }

    public void clickMenuCairkan() throws InterruptedException {
        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/recycler_view_main_menu"));

        // Find all child elements (items) within the RecyclerView
        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/item_layout"));
        if(!items.isEmpty()){
            items.get(1).click();
        }else{
            System.out.println("Tidak ada");
        }
        Thread.sleep(5000);
    }
}
