package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class PinTrxPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/et_otp")
    private MobileElement pinText;

    public PinTrxPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void inputPin(String pin) throws InterruptedException {
        inputTextById(pinText, pin);
    }
}
