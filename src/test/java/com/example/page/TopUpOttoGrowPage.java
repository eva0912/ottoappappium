package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class TopUpOttoGrowPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/et_nominal")
    private MobileElement et_nominal;

    @AndroidFindBy(id = "com.pede.emoney:id/btn_submit")
    private MobileElement btn_submit;
    public TopUpOttoGrowPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void inputNominalIur(String nominal){
        inputTextById(et_nominal, nominal);
    }

    public void clickBtnNext(){
        click(btn_submit);
    }

}
