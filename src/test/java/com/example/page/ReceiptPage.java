package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class ReceiptPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/img_close")
    private MobileElement imgClose;
    @AndroidFindBy(id = "com.pede.emoney:id/img_status")
    private MobileElement imgStatus;
    @AndroidFindBy(id = "com.pede.emoney:id/tv_message")
    private MobileElement tvMessage;
    @AndroidFindBy(id = "com.pede.emoney:id/nestedScrollView")
    private MobileElement nestedScrollView;
    public ReceiptPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
    public void clickClose(){
        click(imgClose);
    }
    public void isVisibleMessage(){
        isElementPresentByMobileElement(tvMessage);
    }
    public void isVisiblenestedScrollView(){
        isElementPresentByMobileElement(nestedScrollView);
    }


}
