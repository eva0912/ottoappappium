package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class PulsaPaketDataPage extends BasePage{

    @AndroidFindBy(id = "com.pede.emoney:id/btn_buy")
    private MobileElement buttonNext;
    @AndroidFindBy(id = "com.pede.emoney:id/et_input_biller")
    private MobileElement etPhoneNumber;
    public PulsaPaketDataPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
    public void inputPhoneNumber(String phoneNumber){
        inputTextById(etPhoneNumber, phoneNumber);
    }
    public void tabDenomPulsa(String price) throws InterruptedException {
        tapMenuByTextContains(price);
    }
    public void clickButtonNext() throws InterruptedException {
        click(buttonNext);
    }
    public void choosetabPascabayar() throws InterruptedException {
        tapMenuByTextContains("Pascabayar");
    }
    public void choosetabData() throws InterruptedException {
        tapMenuByTextContains("Paket Data");
    }
}
