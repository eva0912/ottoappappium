package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class TranferPage extends BasePage {
    @AndroidFindBy(id = "com.pede.emoney:id/btn_tab_1")
    private MobileElement btnTap_transferQris;
    @AndroidFindBy(id = "com.pede.emoney:id/btn_tab_2")
    private MobileElement btnTap_transferteman;

    public TranferPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void clickTabTemanQris(){
        click(btnTap_transferQris);
    }
    public void clickTabTemanOtto(){
        click(btnTap_transferteman);
    }
}
