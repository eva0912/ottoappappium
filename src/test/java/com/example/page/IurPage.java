package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class IurPage extends BasePage{
    @AndroidFindBy(id = "com.pede.emoney:id/img_more_menu")
    private MobileElement img_more_menu;
    public IurPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }

    public void clickMoreIur(){
        click(img_more_menu);
    }
    public void clickJualIur() throws InterruptedException {
        tapMenuByTextContains("Jual");
//        MobileElement recyclerView = driver.findElement(By.id("com.pede.emoney:id/spin_more_menu"));
//
//        // Find all child elements (items) within the RecyclerView
//        List<MobileElement> items = recyclerView.findElements(By.id("com.pede.emoney:id/menu_layout"));
//
//        if(!items.isEmpty()){
//            items.get(1).click();
//            System.out.println("iur "+ items.size());
//        }else{
//            System.out.println("iur Tidak ada");
//        }

    }


}
