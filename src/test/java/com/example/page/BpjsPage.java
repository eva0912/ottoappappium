package com.example.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class BpjsPage extends BasePage{

    @AndroidFindBy(id = "com.pede.emoney:id/et_input_biller")
    private MobileElement editTextbillerNumber;

    @AndroidFindBy(id = "com.pede.emoney:id/iv_biller_next")
    private MobileElement ivNextDenom;

    @AndroidFindBy(id = "com.pede.emoney:id/iv_biller_next")
    private MobileElement btn_buy;
    public BpjsPage(AndroidDriver<AndroidElement> driver_) {
        super(driver_);
        PageFactory.initElements(new AppiumFieldDecorator(driver_), this);
    }
    public void inputBillerNumber(String billNumber){
        inputTextById(editTextbillerNumber, billNumber);
    }
    public void clickPanahGetDenom(){
        click(ivNextDenom);
    }

    public void chooseDenom(String denom) throws InterruptedException {
        tapMenuByTextContains(denom);
    }

    public void clickButtonBuy() throws InterruptedException {
        click(btn_buy);
    }
}
